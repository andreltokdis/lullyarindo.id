import React from 'react'
import Countdown from 'react-countdown'
import { render } from 'react-dom'


// Renderer callback with condition
export default function Countdowns({ hours, minutes, seconds, completed }) {

	const date = '2021-07-01T01:02:03'
	if (completed) {
		// Render a completed state
		return (
			<div>You are good to go!</div>
		)
	} else {
		// Render a countdown
		return (
			<Countdown date={date}
				renderer={(props) => {
						return (
						<span>{props.days} hari {props.hours}:{props.minutes}:{props.seconds}</span>
					)
				}
				}
			/>
		)
	}
}