import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import { DepthOfFieldSnowfall } from 'react-snowflakes'
import { bounce } from 'react-animations'
import Radium, { StyleRoot } from 'radium'
import { Fade } from 'react-slideshow-image'
import dynamic from 'next/dynamic'



export default function Home() {
  const Countdowns = dynamic(
    () => import('../components/countdown'),
    { ssr: false }
  )
  const style = {
    bounce: {
      animation: 'x 2s',
      animationName: Radium.keyframes(bounce, 'bounce'),
      fontSize: 18,
      fontFamily: "Bolds",
      textTransform: 'uppercase',
      color: '#22212c',
      zIndex: 999,
      letterSpacing: 1,
    }
  }

  const images = [
    'https://placeimg.com/640/480/any'
  ];


  return (

    <div className={styles.container}>
      <Head>
        <title>lullyarindo.id - Official Lully & Arindo Wedding Website</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div>
        <div suppressHydrationWarning={true}>
          {process.browser && <DepthOfFieldSnowfall count={50}
            style={{
              // Position must be relative or absolute,
              // because snowflakes are positioned absolutely.
              position: 'absolute',
              width: window.innerWidth,
              height: window.innerHeight,
              zIndex: -100,
              left: '0px',
              top: '0px'
            }} />
          }
        </div>
        <StyleRoot>
          <div style={style.bounce}>
            <h1>Lully | Arindo The Wedding</h1>
          </div>
        </StyleRoot>
        <Countdowns />
        <div className="slide-container">
          <Fade indicators={false} arrows={false}>
            {
              images.map(each => (
                <div style={{ display: 'flex', width: "100%" }} key={each}>
                  <Image key={each} style={{ width: "100%", height: "100%", objectFit: "cover" }} src={each} width={"100%"} height={"100%"} />
                </div>
              )
              )
            }
          </Fade>
        </div>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3963.2673702367774!2d106.80597781539134!3d-6.613665695217412!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69c557852e9415%3A0x959ef307ed89395c!2sVan%20Hoeis%20Bogor!5e0!3m2!1sid!2sid!4v1614213880273!5m2!1sid!2sid"
          width="600"
          height="450"
          style={{ border: 0 }}
          allowFullScreen=""
          loading="lazy"
        />

      </div>
    </div>
  )
}
